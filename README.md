##[Demo APK]

Used: RxJava, Retrofit, Room (for locale storage), DataBinding, LiveData, MVVM, Facebook API 

![one](https://bitbucket.org/killmario/FacebookPhotos/raw/master/screens/1.png)
![one](https://bitbucket.org/killmario/FacebookPhotos/raw/master/screens/2.png)
![one](https://bitbucket.org/killmario/FacebookPhotos/raw/master/screens/3.png)
![one](https://bitbucket.org/killmario/FacebookPhotos/raw/master/screens/4.png)
![one](https://bitbucket.org/killmario/FacebookPhotos/raw/master/screens/5.png)
![one](https://bitbucket.org/killmario/FacebookPhotos/raw/master/screens/6.png)

[Demo APK]: https://bitbucket.org/killmario/facebookphotos/src/master/FacebookPhotos.apk

