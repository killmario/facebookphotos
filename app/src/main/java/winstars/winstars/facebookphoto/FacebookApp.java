package winstars.winstars.facebookphoto;

import android.app.Application;

import winstars.winstars.facebookphoto.data.storage.SessionSharedPreferences;

public class FacebookApp extends Application {

    private static FacebookApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        SessionSharedPreferences.init(this);
    }

    public static FacebookApp getInstance() {
        return instance;
    }
}
