package winstars.winstars.facebookphoto.data.api;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import winstars.winstars.facebookphoto.data.entity.dto.Album;
import winstars.winstars.facebookphoto.data.entity.dto.PageModel;
import winstars.winstars.facebookphoto.data.entity.dto.Photo;

public interface PhotoService {

    @GET("{user-id}/albums")
    Single<PageModel<Album>> getAlbums(@Path("user-id") String id, @Query("access_token") String token);

    @GET("{album-id}/photos")
    Single<PageModel<Photo>> getAlbumPhotos(@Path("album-id") String id, @Query("access_token") String token);
}
