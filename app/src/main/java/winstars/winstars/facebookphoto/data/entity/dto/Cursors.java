package winstars.winstars.facebookphoto.data.entity.dto;

import com.google.gson.annotations.SerializedName;

public class Cursors {

    @SerializedName("after")
    private String after;

    public String getAfter() {
        return after;
    }
}
