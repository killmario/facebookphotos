package winstars.winstars.facebookphoto.views.detail;

import android.arch.lifecycle.LiveData;

import java.util.List;

import winstars.winstars.facebookphoto.FacebookApp;
import winstars.winstars.facebookphoto.data.entity.dao.PhotoEntity;
import winstars.winstars.facebookphoto.data.repository.photos.PhotosRepository;
import winstars.winstars.facebookphoto.views.base.BaseViewModel;

public class DetailsViewModel extends BaseViewModel {

    private LiveData<List<PhotoEntity>> allPhotos;

    public DetailsViewModel(String key) {
        PhotosRepository repository = new PhotosRepository(FacebookApp.getInstance(), key);
        allPhotos = repository.getAllPhotos();
    }

    public LiveData<List<PhotoEntity>> getAllPhotos() {
        return allPhotos;
    }
}
