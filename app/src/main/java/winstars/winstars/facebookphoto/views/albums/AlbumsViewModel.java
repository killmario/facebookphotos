package winstars.winstars.facebookphoto.views.albums;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.List;

import winstars.winstars.facebookphoto.FacebookApp;
import winstars.winstars.facebookphoto.data.entity.dao.AlbumEntity;
import winstars.winstars.facebookphoto.data.entity.dto.Album;
import winstars.winstars.facebookphoto.data.entity.dto.PageModel;
import winstars.winstars.facebookphoto.data.repository.albums.AlbumsRepository;
import winstars.winstars.facebookphoto.utils.Utils;
import winstars.winstars.facebookphoto.views.base.BaseViewModel;

import static winstars.winstars.facebookphoto.data.api.RemoteDataSource.get;

public class AlbumsViewModel extends BaseViewModel {

    private AlbumsRepository repository;
    private LiveData<List<AlbumEntity>> allAlbums;

    public AlbumsViewModel() {
        repository = new AlbumsRepository(FacebookApp.getInstance());
        allAlbums = repository.getAllAlbums();
    }

    public LiveData<List<AlbumEntity>> getAllAlbumsCallback() {
        return allAlbums;
    }

    public void getAlbumsRequest() {

        if (!Utils.isNetworkAvailable(FacebookApp.getInstance())) {
            isRefreshing.set(false);
            return;
        }

        get().getAlbums()
                .subscribe(new BaseSingleObserver<PageModel<Album>>() {
                    @Override
                    public void onSuccess(PageModel<Album> data) {
                        super.onSuccess(data);
                        if (data.getCollection().size() != 0) {
                            List<AlbumEntity> entities = AlbumEntityMapper.apply(data);
                            repository.updateData(entities, null);
                        }
                    }
                });
    }
}
