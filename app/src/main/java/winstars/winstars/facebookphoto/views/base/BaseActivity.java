package winstars.winstars.facebookphoto.views.base;

import android.arch.lifecycle.LifecycleOwner;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity implements LifecycleOwner {

}
