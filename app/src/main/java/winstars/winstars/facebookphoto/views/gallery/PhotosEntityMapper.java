package winstars.winstars.facebookphoto.views.gallery;

import java.util.ArrayList;
import java.util.List;

import winstars.winstars.facebookphoto.BuildConfig;
import winstars.winstars.facebookphoto.data.entity.dao.PhotoEntity;
import winstars.winstars.facebookphoto.data.entity.dto.PageModel;
import winstars.winstars.facebookphoto.data.entity.dto.Photo;

import static winstars.winstars.facebookphoto.data.storage.SessionSharedPreferences.get;

public class PhotosEntityMapper {

    public static List<PhotoEntity> apply(String key, PageModel<Photo> photoPageModel) {
        List<PhotoEntity> entityList = new ArrayList<>();
        for (Photo photo : photoPageModel.getCollection()) {
            PhotoEntity entity = new PhotoEntity();
            entity.setUrl(BuildConfig.BASE_URL + photo.getId() + "/picture?type=normal&access_token=" + get().getToken());
            entity.setAlbumId(key);
            entityList.add(entity);
        }
        return entityList;
    }
}
