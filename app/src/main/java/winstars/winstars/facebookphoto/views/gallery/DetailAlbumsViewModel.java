package winstars.winstars.facebookphoto.views.gallery;

import android.arch.lifecycle.LiveData;

import java.util.List;

import winstars.winstars.facebookphoto.FacebookApp;
import winstars.winstars.facebookphoto.data.entity.dao.PhotoEntity;
import winstars.winstars.facebookphoto.data.entity.dto.PageModel;
import winstars.winstars.facebookphoto.data.entity.dto.Photo;
import winstars.winstars.facebookphoto.data.repository.photos.PhotosRepository;
import winstars.winstars.facebookphoto.utils.Utils;
import winstars.winstars.facebookphoto.views.base.BaseViewModel;

import static winstars.winstars.facebookphoto.data.api.RemoteDataSource.get;

public class DetailAlbumsViewModel extends BaseViewModel {

    private PhotosRepository repository;
    private LiveData<List<PhotoEntity>> allPhotos;

    public DetailAlbumsViewModel(String key) {
        repository = new PhotosRepository(FacebookApp.getInstance(), key);
        allPhotos = repository.getAllPhotos();
    }

    public LiveData<List<PhotoEntity>> getAlbumPhotosCallback() {
        return allPhotos;
    }

    public void getAlbumPhotosRequest(String id) {

        if (!Utils.isNetworkAvailable(FacebookApp.getInstance())) {
            isRefreshing.set(false);
            return;
        }

        get().getAlbumsPhotos(id)
                .subscribe(new BaseSingleObserver<PageModel<Photo>>() {
                    @Override
                    public void onSuccess(PageModel<Photo> data) {
                        super.onSuccess(data);
                        if (data.getCollection().size() != 0) {
                            List<PhotoEntity> entities = PhotosEntityMapper.apply(id, data);
                            repository.updateData(entities, id);
                        }
                    }
                });
    }
}
