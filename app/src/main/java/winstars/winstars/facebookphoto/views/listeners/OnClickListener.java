package winstars.winstars.facebookphoto.views.listeners;

public interface OnClickListener<T> {
    void onClick(T item, int position);
}
