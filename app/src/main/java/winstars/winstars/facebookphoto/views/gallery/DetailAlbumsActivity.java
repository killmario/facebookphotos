package winstars.winstars.facebookphoto.views.gallery;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import winstars.winstars.facebookphoto.R;
import winstars.winstars.facebookphoto.data.entity.dao.PhotoEntity;
import winstars.winstars.facebookphoto.databinding.ActivityMainBinding;
import winstars.winstars.facebookphoto.views.base.BaseActivity;
import winstars.winstars.facebookphoto.views.detail.DetailPhotosActivity;

import static winstars.winstars.facebookphoto.utils.ExtrasKeys.ALBUM_ID;

public class DetailAlbumsActivity extends BaseActivity implements PhotosAdapter.OnPhotoClickListener{

    private ActivityMainBinding binding;
    private DetailAlbumsViewModel viewModel;
    private PhotosAdapter adapter;
    private Intent intent;

    public static void launch(Context context, String id) {
        Intent intent = new Intent(context, DetailAlbumsActivity.class);
        intent.putExtra(ALBUM_ID, id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        intent = getIntent();
        viewModel = new DetailAlbumsViewModel(intent.getStringExtra(ALBUM_ID));
        getLifecycle().addObserver(viewModel);
        setRecycler();
        observeRefresh();
        observeViewModel();
    }

    private void setRecycler() {
        adapter = new PhotosAdapter(this);
        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        binding.recyclerView.setAdapter(adapter);
    }

    private void observeViewModel() {
        viewModel.getAlbumPhotosRequest(intent.getStringExtra(ALBUM_ID));
        viewModel.getAlbumPhotosCallback().observe(this, photos -> adapter.setData(photos));
    }

    private void observeRefresh() {
        binding.swipe.setOnRefreshListener(() -> {
            binding.swipe.setRefreshing(false);
            viewModel.getAlbumPhotosRequest(intent.getStringExtra(ALBUM_ID));
        });
    }

    @Override
    public void onClick(PhotoEntity item, int position) {
        DetailPhotosActivity.launch(this, item.getAlbumId(), position);
    }

    @Override
    public void onLongClick(View view, String url) {
        PreviewPhotoFragment fragment = PreviewPhotoFragment.newInstance(url);
        fragment.show(getFragmentManager(), fragment.getTag());
    }
}
