package winstars.winstars.facebookphoto.views.albums;

import java.util.ArrayList;
import java.util.List;

import winstars.winstars.facebookphoto.BuildConfig;
import winstars.winstars.facebookphoto.data.entity.dao.AlbumEntity;
import winstars.winstars.facebookphoto.data.entity.dto.Album;
import winstars.winstars.facebookphoto.data.entity.dto.PageModel;
import winstars.winstars.facebookphoto.data.storage.SessionSharedPreferences;
import winstars.winstars.facebookphoto.utils.Utils;

public class AlbumEntityMapper {

    public static List<AlbumEntity> apply(PageModel<Album> albumPageModel) {
        List<AlbumEntity> entityList = new ArrayList<>();
        for (Album album : albumPageModel.getCollection()) {
            AlbumEntity entity = new AlbumEntity();
            entity.setId(album.getId());
            entity.setUrl(BuildConfig.BASE_URL + album.getId() + "/picture?access_token=" + SessionSharedPreferences.get().getToken());
            entity.setDate(Utils.parseDate(album.getDate()));
            entity.setName(album.getName());
            entityList.add(entity);
        }
        return entityList;
    }
}
