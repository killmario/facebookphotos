package winstars.winstars.facebookphoto.views.albums;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;

import winstars.winstars.facebookphoto.R;
import winstars.winstars.facebookphoto.data.entity.dao.AlbumEntity;
import winstars.winstars.facebookphoto.databinding.ActivityMainBinding;
import winstars.winstars.facebookphoto.views.base.BaseActivity;
import winstars.winstars.facebookphoto.views.gallery.DetailAlbumsActivity;
import winstars.winstars.facebookphoto.views.listeners.OnClickListener;

public class AlbumsActivity extends BaseActivity implements OnClickListener<AlbumEntity> {

    private ActivityMainBinding binding;
    private AlbumsAdapter adapter;
    private AlbumsViewModel viewModel;

    public static void launch(Context context, boolean isClearTask) {
        Intent intent = new Intent(context, AlbumsActivity.class);
        if (isClearTask) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = new AlbumsViewModel();
        getLifecycle().addObserver(viewModel);
        setRecycler();
        observeRefresh();
        viewModel.getAlbumsRequest();
        viewModel.getAllAlbumsCallback().observe(this, albums -> {
            adapter.setData(albums);
        } );
    }

    private void setRecycler() {
        adapter = new AlbumsAdapter(this);
        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        binding.recyclerView.setAdapter(adapter);
    }

    private void observeRefresh() {
        binding.swipe.setOnRefreshListener(() -> {
            binding.swipe.setRefreshing(false);
            viewModel.getAlbumsRequest();
        });
    }

    @Override
    public void onClick(AlbumEntity item,  int position) {
        DetailAlbumsActivity.launch(this, item.getId());
    }
}
