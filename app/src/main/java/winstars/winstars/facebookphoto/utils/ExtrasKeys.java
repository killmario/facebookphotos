package winstars.winstars.facebookphoto.utils;

public final class ExtrasKeys {
    public static final String ALBUM_ID = "album.id";
    public static final String ITEM_POSITION = "item.position";
}
